<!-- Past Projects Overlay -->
<div id="modal_window-municipal">
  <div class="row">
    <div class="small-11 column past-projects_content">
      <h1>Municipal Project Experience</h1>
      <ul>
        <li>City of Holstein Maintenance, Holstein, IA</li>
        <li>Sac County Landfill Transfer Station, Sac City, IA</li>
        <li>City of Hutchinson Pool Facility, Hutchinson, MN</li>
        <li>St. Charles Fire and Ambulance, St. Charles, MN</li>
        <li>St. Charles Community Center Renovations, St. Charles, MN</li>
        <li>Hinckley Fire Station, Hinckley, MN</li>
        <li>Morris Town Fire Station, Morris town, MN</li>
        <li>City of Belle Plaine Library, Belle Plaine, MN</li>
        <li>Madelia Maintenance Garage, Madelia, MN</li>
        <li>North Mankato Municipal Building ADA Renovation, North Mankato, MN</li>
        <li>City of Victoria City Hall and Library, Victoria, MN</li>
        <li>City of Blue Earth Public Works, Blue Earth, MN</li>
        <li>Tri-County Implement, Jackson, MN</li>
        <li>Waseca Fire Station, Waseca, MN</li>
        <li>Gibbon Water Treatment Center, Gibbon, MN</li>
        <li>Marshall Waste Water Treatment Center, Marshall, MN</li>
        <li>Martin County Sheriff’s Office Renovation, Fairmont, MN</li>
        <li>Harmon Park, Park Shelter, New Ulm, MN </li>
        <li>Lakefield Maintenance, Lakefield, MN </li>
        <li>Jackson Combined Maintenance, Jackson, MN </li>
        <li>Waseca Historical Society, Waseca, MN</li>
        <li>City of Hopkins Community Center HVAC Renovation, Hopkins, MN</li>
        <li>Jackson Fire Station, Jackson, MN</li>
        <li>Pine City Fire Station, Pine City, MN </li>
        <li>Mankato Airport Terminal RTU Replacement, Mankato, MN </li>
        <li>Mayo Hangar, Mankato, MN </li>
        <li>Inter Government Center Renovation, Mankato, MN </li>
        <li>Mankato Public Safety Campus, Mankato, MN </li>
        <li>Lafayette City Hall Bathroom Renovations, Lafayette, MN </li>
        <li>Elmore Library, Elmore, MN </li>
      </ul>
    </div> <!-- / col -->
    <!-- exit -->
    <div class="small-1 column">
      <a id="modal_exit-municipal">
        <img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
      </a>
    </div>
  </div> <!-- / row -->

  <!-- menu footer -->
  <div class="row menu-footer">
    <div class="large-9 column">
      507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
    </div>
    <div class="large-3 column good-thinking">
      Good Thinking.
    </div>
  </div>

</div> <!-- / menu_overlay -->
