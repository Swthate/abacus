<!-- Past Projects Overlay -->
<div id="modal_window-all">
  <div class="row">
    <div class="small-11 column past-projects_content">

      <h1>Project Experience</h1>

      <h2>Educational</h2>
      <ul>
        <li>University of Minnesota Morris Elevator Additions, Morris, MN</li>
        <li>Southwest Minnesota State University Pool Renovation, Marshall, MN</li>
        <li>Minnesota State University Pool Chemical Room Ventilation, Mankato, MN</li>
        <li>Minnesota State University Morris Hall Electrical Panel Replacements, Mankato, MN</li>
        <li>Rochester Public School ESC Restroom Additions, Rochester, MN</li>
        <li>RCTC Restroom Renovations, Rochester, MN</li>
        <li>Jackson Central High School Administrative Area Renovations – Jackson, MN</li>
        <li>Lakefield Middle School Security System Renovation – Lakefield, MN</li>
        <li>Pleasant View Elementary School Office Renovation – Lakefield, MN</li>
        <li>Riverside Elementary School Building Addition – Jackson, MN</li>
        <li>Minneapolis Public School – Franklin Middle School, Minneapolis, MN</li>
        <li>Minneapolis Public School – Hall Elementary School Predesign Analysis, Minneapolis, MN</li>
        <li>Minneapolis Public School – Loring Elementary School Predesign Analysis, Minneapolis, MN</li>
        <li>Minneapolis Public School – Henry High School Predesign Analysis, Minneapolis, MN</li>
        <li>Minnesota State University Library Café, Mankato, MN</li>
        <li>Rochester Community and Technical College/Winona IT Room Renovation, Rochester, MN</li>
        <li>Minnesota State University Crawford Residence Hall Electrical Service Reconfiguration, Mankato, MN</li>
        <li>Minnesota State University Julia Sears Residence Hall Dehumidification, Mankato, MN</li>
        <li>Minnesota State University Residential D-Hall Renovations, Mankato, MN</li>
        <li>Minnesota State University Einstein’s Bagel, Mankato, MN</li>
        <li>Minnesota State University OASIS Office Renovation, Mankato, MN</li>
        <li>Minnesota State University Counseling Center Renovation, Mankato, MN</li>
        <li>Minnesota State University Gage Parking Lot Lighting, Mankato MN</li>
        <li>Minnesota State University Armstrong Hall Basement Renovation, Mankato, MN</li>
        <li>West High School 2nd Floor Renovation, Mankato, MN</li>
        <li>Minnesota State University Presidential Suite Renovation, Mankato, MN</li>
        <li>Minnesota State University Nelson Hall Ramp, Mankato, MN</li>
        <li>Minnesota State University Residential Life C&amp;G Hall Renovations, Mankato, MN</li>
        <li>Minnesota State University Residential Life C&amp;G Hall Renovations Pre-design, Mankato, MN</li>
        <li>Minnesota State University Wiecking Center Print Shop, Mankato, MN</li>
        <li>MVED/North Intermediate Renovation, St. Peter, MN</li>
        <li>Minnesota State University Morris Hall HVAC Renovations, Mankato, MN</li>
        <li>Minnesota State University Highland North HVAC Renovations (Offices, Performing Gym, Dance Hall, Racket Ball Courts, Locker Rooms, etc), Mankato, MN</li>
        <li>Minnesota State University Residential Life E &amp; F Hall Renovations, Mankato, MN</li>
        <li>Minnesota State University Residential Life Elevator Pit Renovations, Mankato, MN</li>
        <li>Minnesota Sear’s Residence Hall A/C Addition, Mankato, MN</li>
        <li>Minnesota State University Nelson Hall Renovations, Mankato, MN</li>
        <li>Minnesota State University Armstrong Hall Renovations, Mankato, MN</li>
        <li>Minnesota State University Highland Center RTU Addition, Mankato, MN</li>
        <li>Minnesota State University Highland North HVAC Upgrade (Gym), Mankato, MN</li>
        <li>Minnesota State University Alumni Foundation RTU Replacements, Mankato, MN</li>
        <li>Minnesota State University LGBT Interior Remodel, Mankato, MN</li>
        <li>Minnesota State University Ostrander Auditorium, Mankato, MN</li>
        <li>Minnesota State University Highland North HVAC Upgrade, Mankato, MN</li>
        <li>Minnesota State University Wigley Administration Building, Mankato, MN</li>
        <li>Minnesota State University Trafton Science Building Lab Remodel, Mankato, MN</li>
      </ul>

      <h2>Municipal</h2>
      <ul>
        <li>City of Holstein Maintenance, Holstein, IA</li>
        <li>Sac County Landfill Transfer Station, Sac City, IA</li>
        <li>City of Hutchinson Pool Facility, Hutchinson, MN</li>
        <li>St. Charles Fire and Ambulance, St. Charles, MN</li>
        <li>St. Charles Community Center Renovations, St. Charles, MN</li>
        <li>Hinckley Fire Station, Hinckley, MN</li>
        <li>Morris Town Fire Station, Morris town, MN</li>
        <li>City of Belle Plaine Library, Belle Plaine, MN</li>
        <li>Madelia Maintenance Garage, Madelia, MN</li>
        <li>North Mankato Municipal Building ADA Renovation, North Mankato, MN</li>
        <li>City of Victoria City Hall and Library, Victoria, MN</li>
        <li>City of Blue Earth Public Works, Blue Earth, MN</li>
        <li>Tri-County Implement, Jackson, MN</li>
        <li>Waseca Fire Station, Waseca, MN</li>
        <li>Gibbon Water Treatment Center, Gibbon, MN</li>
        <li>Marshall Waste Water Treatment Center, Marshall, MN</li>
        <li>Martin County Sheriff’s Office Renovation, Fairmont, MN</li>
        <li>Harmon Park, Park Shelter, New Ulm, MN</li>
        <li>Lakefield Maintenance, Lakefield, MN</li>
        <li>Jackson Combined Maintenance, Jackson, MN</li>
        <li>Waseca Historical Society, Waseca, MN</li>
        <li>City of Hopkins Community Center HVAC Renovation, Hopkins, MN</li>
        <li>Jackson Fire Station, Jackson, MN</li>
        <li>Pine City Fire Station, Pine City, MN</li>
        <li>Mankato Airport Terminal RTU Replacement, Mankato, MN</li>
        <li>Mayo Hangar, Mankato, MN</li>
        <li>Inter Government Center Renovation, Mankato, MN</li>
        <li>Mankato Public Safety Campus, Mankato, MN</li>
        <li>Lafayette City Hall Bathroom Renovations, Lafayette, MN</li>
        <li>Elmore Library, Elmore, MN</li>
      </ul>


      <h2>Apartments and Care Facilities</h2>
      <ul>
        <li>MDK Apartments, Rochester, MN</li>
        <li>Shorewood Wellness Center Building Addition – Rochester, MN</li>
        <li>Briargate Apartments, Mankato, MN</li>
        <li>Stadium Heights Apartments, Mankato, MN</li>
        <li>Bridge Plaza, Mankato, MN</li>
        <li>MDK Apartments, Rochester, MN</li>
        <li>Taylors Falls Assisted Living, Taylors Falls, MN</li>
        <li>Marian Hall, Mankato, MN</li>
        <li>340 Stadium Road, Mankato, MN</li>
        <li>Boatworks Commons, White Bear Lake, MN</li>
        <li>Front Street Mixed Use, Mankato, MN</li>
        <li>Bluff Creek Senior Cottages, Chanhassen, MN</li>
        <li>Lodge at Howard Lake, Howard Lake, MN</li>
        <li>Winthrop Assisted Living, Winthrop, MN</li>
        <li>New Alum Memory Care, New Ulm, MN</li>
        <li>Hutchinson Memory Care, Hutchinson, MN</li>
        <li>Worthington Townhomes, Worthington, MN</li>
        <li>Park Row Housing, St. Peter, MN</li>
        <li>Greenhouse Apartments, Mankato, MN</li>
        <li>James Avenue Apartments, Mankato, MN</li>
        <li>House of Hope Addition, Mankato, MN</li>
        <li>Monks Avenue Mixed Use, Mankato, MN</li>
        <li>Twin Town Villa, Breckenridge, MN</li>
        <li>Gus Johnson and Colonial Square Renovations, Mankato, MN</li>
        <li>Marigold Phase II, North Mankato, MN</li>
        <li>Welcome Manor, Garden City, MN</li>
        <li>225 Belgrade Avenue, North Mankato, MN</li>
        <li>Lakeside Manor, Redwood Falls, MN</li>
        <li>Mountain Lake 4-Plex, Mountain Lake, MN</li>
      </ul>


      <h2>Sports and Community Facilities</h2>
      <ul>
        <li>Treasure Island Pool Facility, Welch, MN</li>
        <li>City of Hutchinson Pool Facility, Hutchinson, MN</li>
        <li>Parkers Prairie Pool Facility, Parkers Prairie, MN</li>
        <li>Rochester Senior Rec Center Pool Facility, Rochester, MN</li>
        <li>JM Marriott at Mall of America Pool Facility, Bloomington, MN</li>
        <li>New Prague Pool, New Prague, MN</li>
        <li>Luxembourg Apartment Pool Facility, Luxembourg, MN</li>
        <li>Kamp Dels Pool Addition, Waterville, MN</li>
        <li>Ramada Inn Ohio Pool, Elyria, OH</li>
        <li>Cascade Bay Water Park, Eagan, MN</li>
        <li>Motevideo Water Park, Montevideo, MN</li>
        <li>Franklin School Swimming Pool, Gym and Locker Room, Minneapolis, MN</li>
        <li>YMCA Swimming Pool, Mankato, MN</li>
        <li>Minnesota State University Highland North HVAC Renovations (Offices, Performing Gym, Dance Hall, Racket Ball Courts, Locker Rooms, etc), Mankato, MN</li>
      </ul>


      <h2>Offices</h2>
      <ul>
        <li>Landmark Realtor, Mankato, MN</li>
        <li>Braam Office Building, Mankato, MN</li>
        <li>Bridge Plaza, Mankato, MN</li>
        <li>Abdo Eick &amp; Meyers, Mankato, MN</li>
        <li>Blethen, Gage and Krause, Mankato, MN</li>
        <li>New Brighton Barrier Free, New Brighton, MN</li>
        <li>Furnbrook Office Building, Owatonna, MN</li>
        <li>MRCI Eastwood, Mankato, MN</li>
        <li>Profinium, Mankato, MN</li>
        <li>Nichols Building Renovation, Mankato, MN</li>
        <li>Minnesota Valley Action Council, Mankato, MN</li>
        <li>Bretts Building Suite 150, Mankato, MN</li>
        <li>FPX Office Remodel, Bloomington, MN</li>
        <li>Hugo Building, Mankato, MN</li>
        <li>Zachett Office Building, Mankato, MN</li>
        <li>225 Belgrade Avenue, North Mankato, MN</li>
        <li>MRCI Renovations, Mankato, MN</li>
      </ul>


      <h2>Industrial</h2>
      <ul>
        <li>DK Powder Coating, North Mankato, MN</li>
        <li>Kato Cable, Mankato, MN</li>
        <li>UFC Agronomy, Winthrop, MN</li>
        <li>Briargate Maintenance Building, Mankato, MN</li>
        <li>Kibble Equipment, Minnesota Lake, MN</li>
        <li>Building Fasteners, North Mankato, MN</li>
        <li>Madelia Maintenance Garage, Madelia, MN</li>
        <li>Mountain Lake Power, Mountain Lake, MN</li>
        <li>Tri-County Implement, Jackson, MN</li>
        <li>Tony Downs Food, Madelia, MN</li>
        <li>Kahler Automation, Fairmont, MN</li>
        <li>MRCI Eastwood, Mankato, MN</li>
        <li>Ten E, Newport, MN</li>
        <li>Arnolds Implement, North Mankato, MN</li>
        <li>Olson Truck Service Center, St. James, MN</li>
        <li>MRCI Office Area Renovation, Mankato MN</li>
        <li>Kato Engineering, Mankato, MN</li>
        <li>Mayo Hangar, Mankato, MN</li>
        <li>Accraply Steam Extraction, Plymouth MN</li>
      </ul>


      <h2>Healthcare</h2>
      <ul>
        <li>South Minnesota Oral Maxillofacial Surgery, Mankato, MN</li>
        <li>Mayo Clinic AHU Replacement, Fairmont, MN</li>
        <li>Gentle Dental, Owatonna, MN</li>
        <li>Mayo Clinic Renovation, Albert Lea, MN</li>
        <li>Open Door Clinic, Mankato, MN</li>
        <li>Mankato Clinic Ultrasound, Mankato, MN</li>
        <li>Mankato Surgery Center, Mankato, MN</li>
        <li>Lake Crystal Clinic, Lake Crystal, MN</li>
        <li>New Ulm Medical Center, New Ulm, MN</li>
        <li>Mercy Hospital RTU Addition, Coon Rapids, MN</li>
      </ul>


      <h2>Liturgical</h2>
      <ul>
        <li>Mountain Lake Assembly of God, Mountain Lake, MN</li>
        <li>Newman Center, Mankato, MN</li>
        <li>Risen Savior Church, Mankato, MN</li>
        <li>Trinity Lutheran Church, Janesville, MN</li>
        <li>Elevate Church, Mankato, MN</li>
        <li>St. John the Baptist Catholic Church, Mankato, MN</li>
        <li>Hosanna Lutheran Church, Mankato, MN</li>
      </ul>


      <h2>Spa and Salons</h2>
      <ul>
        <li>Treasure Island Spa and Salon, Welch, MN</li>
        <li>Edina Nail Salon, Edina, MN</li>
        <li>Chanhassen Nail Salon, Chanhassen, MN</li>
        <li>Salon Suites, Mankato, MN</li>
        <li>Body Beautiful Spa, Mankato, MN</li>
        <li>Hostess Building Renovations (New Nail Salon), Mankato, MN</li>
        <li>Buzz Hair Salon Renovations, Mankato, MN</li>
      </ul>


      <h2>Restaurants</h2>
      <ul>
        <li>Polito’s Pizza, Mankato, MN</li>
        <li>Taco Bell / Pizza Hut, Viera, FL</li>
        <li>Toco Bell, Williston, FL</li>
        <li>Taco Bell / Pizza Hut, Windermere, FL</li>
        <li>Minnesota State University Library Café, Mankato, MN</li>
        <li>Dairy Queen and Tenant Spaces, Mankato, MN</li>
        <li>Burger King, Duluth, MN</li>
        <li>Einstein’s Bagels, Mankato, MN</li>
        <li>Little Caesars, Owatonna, MN</li>
        <li>Starbucks, Mankato, MN</li>
        <li>Owatonna Retail Center, Owatonna, MN</li>
        <li>Sassy Strawberries, Austin MN, Burlington, IA, Waterloo, IA</li>
        <li>Subway, Mankato, MN</li>
        <li>Massads, Mankato MN</li>
        <li>La Terraza, Mankato, MN</li>
        <li>Tandem Bagels, Mankato, MN</li>
        <li>KFC/Taco Bell, New Ulm, MN</li>
        <li>Lava Java Renovation, Mankato, MN</li>
        <li>Jake’s Stadium Pizza, St. Peter, MN</li>
        <li>Pho Sigon Restaurant, Mankato, MN</li>
        <li>St. Peter Food Co-op, St. Peter, MN</li>
      </ul>


      <h2>Automotive Dealership and Repair</h2>
      <ul>
        <li>Mankato Quick Lube, Mankato, MN</li>
        <li>Luther Honda Renovations, Mankato, MN</li>
        <li>Ron’s Repair, Worthington, MN</li>
      </ul>


      <h2>Energy Audit Reports / Predesign Report / Feasibility Study Reports</h2>
      <ul>
        <li>Minneapolis Public School – Hall Elementary School Predesign Analysis, Minneapolis, MN</li>
        <li>Minneapolis Public School – Loring Elementary School Predesign Analysis, Minneapolis, MN</li>
        <li>Minneapolis Public School – Henry High School Predesign Analysis, Minneapolis, MN</li>
        <li>Marian Hall Building Assessment and Feasibility Study Report – Mankato, MN</li>
        <li>Building Assessment Report, Blue Earth, MN</li>
        <li>Predesign Report – MSU Armstrong Hall Basement HVAC, Mankato, MN</li>
        <li>Energy Audit Report - Coloplast, North Mankato, MN</li>
        <li>System Verification Report – Berry Blends, Mankato, MN</li>
        <li>System Verification Report – MTU Onsite, Mankato, MN</li>
      </ul>


    </div> <!-- / col -->
    <div class="small-1 column">
      <!-- exit -->
      <a id="modal_exit-all">
        <img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
      </a>
    </div>
  </div> <!-- / row -->

  <!-- menu footer -->
  <div class="row menu-footer">

    <div class="large-9 column">
      507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
    </div>

    <div class="large-3 column good-thinking">
      Good Thinking.
    </div>

  </div>

</div> <!-- / menu_overlay -->
