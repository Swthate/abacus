<!-- Past Projects Overlay -->
<div id="modal_window-community-sports">
  <div class="row">
    <div class="small-11 column past-projects_content">
      <h1>Community &amp; Sports Project Experience</h1>
      <ul>
        <li>Treasure Island Pool Facility, Welch, MN</li>
        <li>City of Hutchinson Pool Facility, Hutchinson, MN</li>
        <li>Parkers Prairie Pool Facility, Parkers Prairie, MN</li>
        <li>Rochester Senior Rec Center Pool Facility, Rochester, MN</li>
        <li>JM Marriott at Mall of America Pool Facility, Bloomington, MN</li>
        <li>New Prague Pool, New Prague, MN</li>
        <li>Luxembourg Apartment Pool Facility, Luxembourg, MN</li>
        <li>Kamp Dels Pool Addition, Waterville, MN</li>
        <li>Ramada Inn Ohio Pool, Elyria, OH</li>
        <li>Cascade Bay Water Park, Eagan, MN</li>
        <li>Motevideo Water Park, Montevideo, MN</li>
        <li>Franklin School Swimming Pool, Gym and Locker Room, Minneapolis, MN</li>
        <li>YMCA Swimming Pool, Mankato, MN</li>
        <li>Minnesota State University Highland North HVAC Renovations (Offices, Performing Gym, Dance Hall, Racket Ball Courts, Locker Rooms, etc), Mankato, MN</li>
      </ul>
    </div> <!-- / col -->
    <div class="small-1 column">
      <!-- exit -->
      <a id="modal_exit-community-sports">
        <img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
      </a>
    </div>
  </div> <!-- / row -->

  <!-- menu footer -->
  <div class="row menu-footer">

    <div class="large-9 column">
      507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
    </div>

    <div class="large-3 column good-thinking">
      Good Thinking.
    </div>

  </div>

</div> <!-- / menu_overlay -->
