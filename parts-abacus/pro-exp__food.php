<!-- Past Projects Overlay -->
<div id="modal_window-food-restaurant">
  <div class="row">
    <div class="small-11 column past-projects_content">
      <h1>Food &amp; Restaurant Project Experience</h1>
      <ul>
        <li>Polito’s Pizza, Mankato, MN</li>
        <li>Taco Bell / Pizza Hut, Viera, FL</li>
        <li>Toco Bell, Williston, FL</li>
        <li>Taco Bell / Pizza Hut, Windermere, FL</li>
        <li>Minnesota State University Library Café, Mankato, MN</li>
        <li>Dairy Queen and Tenant Spaces, Mankato, MN</li>
        <li>Burger King, Duluth, MN</li>
        <li>Einstein’s Bagels, Mankato, MN</li>
        <li>Little Caesars, Owatonna, MN</li>
        <li>Starbucks, Mankato, MN</li>
        <li>Owatonna Retail Center, Owatonna, MN</li>
        <li>Sassy Strawberries, Austin MN, Burlington, IA, Waterloo, IA</li>
        <li>Subway, Mankato, MN</li>
        <li>Massads, Mankato MN</li>
        <li>La Terraza, Mankato, MN</li>
        <li>Tandem Bagels, Mankato, MN</li>
        <li>KFC/Taco Bell, New Ulm, MN </li>
        <li>Lava Java Renovation, Mankato, MN </li>
        <li>Jake’s Stadium Pizza, St. Peter, MN </li>
        <li>Pho Sigon Restaurant, Mankato, MN </li>
        <li>St. Peter Food Co-op, St. Peter, MN </li>
      </ul>
    </div> <!-- / col -->
    <!-- exit -->
    <div class="small-1 column">
      <a id="modal_exit-food-restaurant">
        <img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
      </a>
    </div>
  </div> <!-- / row -->

  <!-- menu footer -->
  <div class="row menu-footer">
    <div class="large-9 column">
      507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
    </div>
    <div class="large-3 column good-thinking">
      Good Thinking.
    </div>
  </div>

</div> <!-- / menu_overlay -->
