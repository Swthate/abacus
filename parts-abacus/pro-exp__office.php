<!-- Past Projects Overlay -->
<div id="modal_window-office">
  <div class="row">
    <div class="small-11 column past-projects_content">
      <h1>Office Project Experience</h1>
      <ul>
        <li>Landmark Realtor, Mankato, MN</li>
        <li>Braam Office Building, Mankato, MN</li>
        <li>Bridge Plaza, Mankato, MN</li>
        <li>Abdo Eick &amp; Meyers, Mankato, MN</li>
        <li>Blethen, Gage and Krause, Mankato, MN</li>
        <li>New Brighton Barrier Free, New Brighton, MN</li>
        <li>Furnbrook Office Building, Owatonna, MN</li>
        <li>MRCI Eastwood, Mankato, MN </li>
        <li>Profinium, Mankato, MN </li>
        <li>Nichols Building Renovation, Mankato, MN</li>
        <li>Minnesota Valley Action Council, Mankato, MN </li>
        <li>Bretts Building Suite 150, Mankato, MN </li>
        <li>FPX Office Remodel, Bloomington, MN</li>
        <li>Hugo Building, Mankato, MN </li>
        <li>Zachett Office Building, Mankato, MN</li>
        <li>225 Belgrade Avenue, North Mankato, MN</li>
        <li>MRCI Renovations, Mankato, MN</li>
      </ul>
    </div> <!-- / col -->
    <!-- exit -->
    <div class="small-1 column">
      <a id="modal_exit-office">
        <img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
      </a>
    </div>
  </div> <!-- / row -->

  <!-- menu footer -->
  <div class="row menu-footer">
    <div class="large-9 column">
      507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
    </div>
    <div class="large-3 column good-thinking">
      Good Thinking.
    </div>
  </div>

</div> <!-- / menu_overlay -->
