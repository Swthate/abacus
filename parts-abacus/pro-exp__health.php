<!-- Past Projects Overlay -->
<div id="modal_window-health-care">
  <div class="row">
    <div class="small-11 column past-projects_content">
      <h1>Health Care Project Experience</h1>
      <ul>
        <li>South Minnesota Oral Maxillofacial Surgery, Mankato, MN</li>
        <li>Mayo Clinic AHU Replacement, Fairmont, MN</li>
        <li>Gentle Dental, Owatonna, MN</li>
        <li>Mayo Clinic Renovation, Albert Lea, MN</li>
        <li>Open Door Clinic, Mankato, MN</li>
        <li>Mankato Clinic Ultrasound, Mankato, MN</li>
        <li>Mankato Surgery Center, Mankato, MN</li>
        <li>Lake Crystal Clinic, Lake Crystal, MN </li>
        <li>New Ulm Medical Center, New Ulm, MN </li>
        <li>Mercy Hospital RTU Addition, Coon Rapids, MN</li>
      </ul>
    </div> <!-- / col -->
    <!-- exit -->
    <div class="small-1 column">
      <a id="modal_exit-health-care">
        <img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
      </a>
    </div>
  </div> <!-- / row -->

  <!-- menu footer -->
  <div class="row menu-footer">
    <div class="large-9 column">
      507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
    </div>
    <div class="large-3 column good-thinking">
      Good Thinking.
    </div>
  </div>

</div> <!-- / menu_overlay -->
