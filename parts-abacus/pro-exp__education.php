<!-- Past Projects Overlay -->
<div id="modal_window-education">
  <div class="row">
    <div class="small-11 column past-projects_content">

      <h1>Education Project Experience</h1>

        <ul>
          <li>University of Minnesota Morris Elevator Additions, Morris, MN</li>
          <li>Southwest Minnesota State University Pool Renovation, Marshall, MN</li>
          <li>Minnesota State University Pool Chemical Room Ventilation, Mankato, MN</li>
          <li>Minnesota State University Morris Hall Electrical Panel Replacements, Mankato, MN</li>
          <li>Rochester Public School ESC Restroom Additions, Rochester, MN</li>
          <li>RCTC Restroom Renovations, Rochester, MN</li>
          <li>Jackson Central High School Administrative Area Renovations – Jackson, MN</li>
          <li>Lakefield Middle School Security System Renovation – Lakefield, MN</li>
          <li>Pleasant View Elementary School Office Renovation – Lakefield, MN</li>
          <li>Riverside Elementary School Building Addition – Jackson, MN </li>
          <li>Minneapolis Public School – Franklin Middle School, Minneapolis, MN </li>
          <li>Minneapolis Public School – Hall Elementary School Predesign Analysis, Minneapolis, MN</li>
          <li>Minneapolis Public School – Loring Elementary School Predesign Analysis, Minneapolis, MN</li>
          <li>Minneapolis Public School – Henry High School Predesign Analysis, Minneapolis, MN</li>
          <li>Minnesota State University Library Café, Mankato, MN</li>
          <li>Rochester Community and Technical College/Winona IT Room Renovation, Rochester, MN</li>
          <li>Minnesota State University Crawford Residence Hall Electrical Service Reconfiguration, Mankato, MN</li>
          <li>Minnesota State University Julia Sears Residence Hall Dehumidification, Mankato, MN</li>
          <li>Minnesota State University Residential D-Hall Renovations, Mankato, MN</li>
          <li>Minnesota State University Einstein’s Bagel, Mankato, MN</li>
          <li>Minnesota State University OASIS Office Renovation, Mankato, MN</li>
          <li>Minnesota State University Counseling Center Renovation, Mankato, MN</li>
          <li>Minnesota State University Gage Parking Lot Lighting, Mankato MN</li>
          <li>Minnesota State University Armstrong Hall Basement Renovation, Mankato, MN</li>
          <li>West High School 2nd Floor Renovation, Mankato, MN</li>
          <li>Minnesota State University Presidential Suite Renovation, Mankato, MN </li>
          <li>Minnesota State University Nelson Hall Ramp, Mankato, MN </li>
          <li>Minnesota State University Residential Life C&amp;G Hall Renovations, Mankato, MN </li>
          <li>Minnesota State University Residential Life C&amp;G Hall Renovations Pre-design, Mankato, MN </li>
          <li>Minnesota State University Wiecking Center Print Shop, Mankato, MN </li>
          <li>MVED/North Intermediate Renovation, St. Peter, MN </li>
          <li>Minnesota State University Morris Hall HVAC Renovations, Mankato, MN </li>
          <li>Minnesota State University Highland North HVAC Renovations (Offices, Performing Gym, Dance Hall, Racket Ball Courts, Locker Rooms, etc), Mankato, MN</li>
          <li>Minnesota State University Residential Life E &amp; F Hall Renovations, Mankato, MN </li>
          <li>Minnesota State University Residential Life Elevator Pit Renovations, Mankato, MN </li>
          <li>Minnesota Sear’s Residence Hall A/C Addition, Mankato, MN </li>
          <li>Minnesota State University Nelson Hall Renovations, Mankato, MN</li>
          <li>Minnesota State University Armstrong Hall Renovations, Mankato, MN</li>
          <li>Minnesota State University Highland Center RTU Addition, Mankato, MN</li>
          <li>Minnesota State University Highland North HVAC Upgrade (Gym), Mankato, MN </li>
          <li>Minnesota State University Alumni Foundation RTU Replacements, Mankato, MN </li>
          <li>Minnesota State University LGBT Interior Remodel, Mankato, MN </li>
          <li>Minnesota State University Ostrander Auditorium, Mankato, MN </li>
          <li>Minnesota State University Highland North HVAC Upgrade, Mankato, MN</li>
          <li>Minnesota State University Wigley Administration Building, Mankato, MN</li>
          <li>Minnesota State University Trafton Science Building Lab Remodel, Mankato, MN</li>
        </ul>
      </div> <!-- / col -->
      <div class="small-1 column">
        <!-- exit -->
        <a id="modal_exit-education">
          <img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
        </a>
      </div>
    </div> <!-- / row -->

    <!-- menu footer -->
    <div class="row menu-footer">

      <div class="large-9 column">
        507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
      </div>

      <div class="large-3 column good-thinking">
        Good Thinking.
      </div>

    </div>

  </div> <!-- / menu_overlay -->
