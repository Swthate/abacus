<?php

get_header(); ?>


<?php
/**
 * To cleaer things up on this page, we're moving all the clutter for the
 * Past Projects modal to a separte file:
 **/
	include ( 'parts-abacus/pro-exp__all.php' );
	include ( 'parts-abacus/pro-exp__community.php' );
	include ( 'parts-abacus/pro-exp__education.php' );
	include ( 'parts-abacus/pro-exp__food.php' );
	include ( 'parts-abacus/pro-exp__health.php' );
	include ( 'parts-abacus/pro-exp__municipal.php' );
	include ( 'parts-abacus/pro-exp__office.php' );
?>



<div class="row" id="search-filter-results-container">
	<div class="large-12 medium-11 medium-centered small-11 small-centered column main-content" role="main">



		<!-- Page Introduction -->
		<h2>Our Work.</h2>
		<p class="sub-h2">Trained in Leed, B3, Energy Star and Enterprise Green Community.</p>



		<!-- Project Category Filters -->
		<?php include ( 'work-category-filter.php' ); ?>



		<!-- Start block grid -->
		<!-- Block grid starts BEFORE loop so we can display project experience list items even when
		     the loop is empty! -->
		<ul class="large-block-grid-2 medium-block-grid-1 small-block-grid-1" id="search-filter-results-container">



		<!-- Setting Up The Loop -->
		<?php if ( have_posts() ) : ?>
		<?php do_action( 'foundationpress_before_content' ); ?>



		<!-- The Loop Begins! -->
		<?php while ( have_posts() ) : the_post(); ?>
		<?php $bg = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );  ?>
		<?php include 'functions-portfolio.php'; ?>


			<!-- Each looped item is a list item inside the block-grid <ul> -->
			<li>

				<!-- Project Image -->
				<a href="<?php the_permalink();?>">
					<div class="portfolio-item" style="background-image:url('<?php echo $bg[0]; ?>')"></div>
				</a>

				<!-- Project Title and Details -->
				<div class="item__title">
					<h3><?php the_title(); ?></h3>
					<a href="<?php the_permalink();?>">
						<p class="basic-link">Click to View Project</p>
					</a>
					<a href id="<?php echo $modal_enter_cat;?>">
						<p><?php echo $work_category; ?></p>
					</a>
				</div>

			</li>


		<!-- Ending The Loop -->
		<?php endwhile; ?>

	<?php do_action( 'foundationpress_before_pagination' ); ?>

	<?php endif;?>

	<!-- Block outside of loop for Past Project List -->
	<?php include ( 'work-project-experience.php' ); ?>

	</ul> <!-- End block grid -->

  <?php // Reset the main query ?>
  <?php wp_reset_query(); ?>



	<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php } ?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div> <!-- / main column -->
</div> <!-- / main row -->
<?php get_footer(); ?>
