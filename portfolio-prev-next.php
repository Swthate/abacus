<!-- Projects Navigation -->
<div class="single-post-pagination--off">
  <div class="row">

    <!-- Previous Post -->
    <div class="small-4 column text-right">
      &nbsp;
      <?php previous_post_link('%link','<img src="http://thate.me/proofs/abacus/wp-content/themes/abacus-foundationpress/assets/img/svg/arrow-left.svg" alt="Previous Project">'); ?>
    </div>

    <!-- Project Index -->
    <div class="small-4 column text-center">
      <a href="<?php bloginfo('url');?>/portfolio">
        <img src="http://thate.me/proofs/abacus/wp-content/themes/abacus-foundationpress/assets/img/svg/grid.svg" alt="Work Index">
      </a>
    </div>

    <!-- Next Post -->
    <div class="small-4 column text-left">
      <?php next_post_link('%link',"<img src='http://thate.me/proofs/abacus/wp-content/themes/abacus-foundationpress/assets/img/svg/arrow-right.svg' alt='Next Project'>"); ?>
    </div>

  </div> <!-- / row -->
</div> <!-- / single post navigation -->
