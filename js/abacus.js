/**
 * Sticky Navigation
 *
 **/





// 100% width of Body
var fullWidth = $('body').width();

$('#menu-modal').bind('reveal:opened', function(){
var menu = $('#menu-modal');

  // Upon opening, assign Body width to #menu-modal
  // The reason for this is hidden elements don't listen to widths!
  menu.css('width', '100vw');

});





/**
 * Toggle Menu Button (version 1)
 *
 *
var menuBtn = $('#menu-button');
var closeBtn = $('.close-reveal-modal');

// Hide menu button when clicked.
$(function() {
  $('#menu-button').click(function() {
    $(this).css('display', 'none');
  });
});

// Show menu button when modal close button clicked.
$(function() {
  $('.close-reveal-modal').click(function() {
    $('#menu-button').hide().css("visibility", "visible").fadeIn("slow");
  });
});
*/





/**
 * Toggle Menu (version 2)
 *
 **/

jQuery(function($){
  jQuery('#menu_enter, #menu_exit').click(function(e){
    e.preventDefault();
    jQuery('#menu_overlay').toggle('300');
  });
});





/**
 * Toggle Past Projects Modal
 *
 * community-sports
 * education
 * food-restaurant
 * health-care
 * municipal
 * office
 **/

jQuery(function($){
  jQuery('#modal_enter-all, #modal_exit-all').click(function(e){
    e.preventDefault();
    jQuery('#modal_window-all').toggle('300');
  });
});

// Community & Sports
jQuery(function($){
  jQuery('#modal_enter-community-sports, #modal_exit-community-sports').click(function(e){
    e.preventDefault();
    jQuery('#modal_window-community-sports').toggle('300');
  });
});

// Education
jQuery(function($){
  jQuery('#modal_enter-education, #modal_exit-education').click(function(e){
    e.preventDefault();
    jQuery('#modal_window-education').toggle('300');
  });
});

// Food & Restaurant
jQuery(function($){
  jQuery('#modal_enter-food-restaurant, #modal_exit-food-restaurant').click(function(e){
    e.preventDefault();
    jQuery('#modal_window-food-restaurant').toggle('300');
  });
});

// Health Care
jQuery(function($){
  jQuery('#modal_enter-health-care, #modal_exit-health-care').click(function(e){
    e.preventDefault();
    jQuery('#modal_window-health-care').toggle('300');
  });
});

// Municipal
jQuery(function($){
  jQuery('#modal_enter-municipal, #modal_exit-municipal').click(function(e){
    e.preventDefault();
    jQuery('#modal_window-municipal').toggle('300');
  });
});

// Office
jQuery(function($){
  jQuery('#modal_enter-office, #modal_exit-office').click(function(e){
    e.preventDefault();
    jQuery('#modal_window-office').toggle('300');
  });
});



/**
 * Fade Team Leadership details on hover
 *
 **/

jQuery(function($) {
    jQuery('.team-member').hover(function() {
        jQuery('.member__details').fadeIn('slow');
      },
      function($){
        jQuery('.member__details').fadeOut('slow');
      }
   );
});





/**
 * Prev/Next Portfolio Posts
 *
 * Prev / next buttons for portfolio items do not initially appear. They
 * should only appear after the user has scrolled X number of pixels.
 *
 * 1. Enter the number of pixels to scroll before displaying prev / next
 * buttons.
 *
 **/

$(window).scroll(function() {
  var scroll = $(window).scrollTop();

  if (scroll >= 105) { /* [1] */
    $(".single-post-pagination--off").fadeIn(300, function() {
      $(".single-post-pagination--off").addClass("single-post-pagination--on");
    });
  } else {
    $(".single-post-pagination--off").fadeOut(300, function() {
      $(".single-post-pagination--off").removeClass("single-post-pagination--on");
    });
  }
});
