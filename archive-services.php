<?php

get_header(); ?>

<div class="row">
	<div class="large-12 medium-11 medium-centered small-11 small-centered column main-content" role="main">


		<h2>Our Services.</h2>
		<p class="sub-h2">Innovative, cost effective and energy efficient. Trained in LEED, B3, Energy Star and Enterprise Green Community.</p>
</br>

  <?php
    $args = array( 'post_type' => 'services' );
    $wp_query = new WP_Query( $args );
  ?>

	<?php if ( have_posts() ) : ?>
	<?php do_action( 'foundationpress_before_content' ); ?>


	<?php while ( have_posts() ) : the_post(); ?>
	<?php $bg = get_field('photo'); ?>



		<div class="row services">
      <div class="medium-6 column">
        <div class="services__photo" style="background-image:url('<?php echo $bg; ?>')"></div>
      </div>
      <div class="medium-6 column">
        <div class="services__copy">
          <h3><?php the_title(); ?></h3>
          <?php the_content(); ?>
        </div>
      </div>
    </div> <!-- / row -->




	<!-- End While Loop -->
	<?php endwhile; ?>

	</ul> <!-- End block grid -->





	<?php do_action( 'foundationpress_before_pagination' ); ?>

	<?php endif;?>

  <?php // Reset the main query ?>
  <?php wp_reset_query(); ?>



	<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php } ?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div> <!-- / main column -->
</div> <!-- / main row -->
<?php get_footer(); ?>
