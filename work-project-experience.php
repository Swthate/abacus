<?php $active_category = get_query_var( '_sfm_work_category' ); ?>



  <!-- All Categories -->
  <?php if( !$active_category ) : ?>
    <li>
        <div class="portfolio-item" style="background-image:url('<?php bloginfo('template_url');?>/assets/img/work/project_list_image.jpg')">
        </div>
        <div class="item__title">
          <h3>Project Experience</h3>
          <a href id="modal_enter-all">
            <p class="basic-link">Click to View All Projects</p>
          </a>
        </div>
    </li>
  <?php endif; ?>



  <!-- Community & Sports -->
  <?php if( $active_category === "Community & Sports" ) : ?>
    <li>
        <div class="portfolio-item" style="background-image:url('<?php bloginfo('template_url');?>/assets/img/work/project_list_image.jpg')">
        </div>
        <div class="item__title">
          <h3>Community &amp; Sports Project Experience</h3>
          <a href id="<?php echo $modal_enter_cat;?>">
            <p class="basic-link">Click to View All Community &amp; Sports Projects</p>
          </a>
        </div>
    </li>
  <?php endif; ?>



  <!-- Education -->
  <?php if( $active_category === "Education" ) : ?>
    <li>
        <div class="portfolio-item" style="background-image:url('<?php bloginfo('template_url');?>/assets/img/work/project_list_image.jpg')">
        </div>
        <div class="item__title">
          <h3>Education Project Experience</h3>
          <a href id="<?php echo $modal_enter_cat;?>">
            <p class="basic-link">Click to View All Education Projects</p>
          </a>
        </div>
    </li>
  <?php endif; ?>



  <!-- Food & Restaurants -->
  <?php if( $active_category === "Food & Restaurant" ) : ?>
    <li>
        <div class="portfolio-item" style="background-image:url('<?php bloginfo('template_url');?>/assets/img/work/project_list_image.jpg')">
        </div>
        <div class="item__title">
          <h3>Food &amp; Restaurant Project Experience</h3>
          <a href id="<?php echo $modal_enter_cat;?>">
            <p class="basic-link">Click to View All Food &amp; Restaurant Projects</p>
          </a>
        </div>
    </li>
  <?php endif; ?>



  <!-- Health Care -->
  <?php if( $active_category === "Health Care" ) : ?>
    <li>
        <div class="portfolio-item" style="background-image:url('<?php bloginfo('template_url');?>/assets/img/work/project_list_image.jpg')">
        </div>
        <div class="item__title">
          <h3>Health Care Project Experience</h3>
          <a href id="<?php echo $modal_enter_cat;?>">
            <p class="basic-link">Click to View All Health Care Projects</p>
          </a>
        </div>
    </li>
  <?php endif; ?>



  <!-- Municipal -->
  <?php if( $active_category === "Municipal" ) : ?>
    <li>
        <div class="portfolio-item" style="background-image:url('<?php bloginfo('template_url');?>/assets/img/work/project_list_image.jpg')">
        </div>
        <div class="item__title">
          <h3>Municipal Project Experience</h3>
          <a href id="<?php echo $modal_enter_cat;?>">
            <p class="basic-link">Click to View All Municipal Projects</p>
          </a>
        </div>
    </li>
  <?php endif; ?>



  <!-- Office -->
  <?php if( $active_category === "Office" ) : ?>
    <li>
        <div class="portfolio-item" style="background-image:url('<?php bloginfo('template_url');?>/assets/img/work/project_list_image.jpg')">
        </div>
        <div class="item__title">
          <h3>Office Project Experience</h3>
          <a href id="<?php echo $modal_enter_cat;?>">
            <p class="basic-link">Click to View All Office Projects</p>
          </a>
        </div>
    </li>
  <?php endif; ?>
