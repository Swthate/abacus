Work Log for Abacus WordPress Theme.

*(billed) means the time has been tallied on Active.Collab*
-----------------------------------------------------------

**Tue Dec 29 2015**

11:15am -
* Continue arrow and grid icons.

10:00am - 10:30am
* Update arrow and grid icons for portfolio pagination.
* Break for breakfast

-----------------------------------------------------------

**Mon Dec 28 2015**

10:00pm - 11:00pm *billed*
* Working on contextual past projects lists.
* Contextual project experience lists added. The code is beyond ugly... Needs cleaning and automating whenever possible.

2:00pm - 2:30pm *billed*
* Working on contextual past projects lists.

-----------------------------------------------------------

**Tue Dec 22 2015**

11:00pm - 12:00am *billed*
* Downloaded updated pages from FTP (changes made by Bayard).
* Change paragraphs on Services page to left alignment (from justified).
* On portfolio index, make "Click to..." an obvious link. Orange with underline.
* Pagination arrows on single portfolio post pages display after user has
  scrolled down a bit.

-----------------------------------------------------------

**Sat Dec 19 2015**

5:20pm - 5:30pm
* Update prev/next links. Change post title links to arrow icons. Change "grid"
  icon to font awesome icon.

1:00pm - 1:30pm *billed*
* Add Home and About Us to menu.
* Fix menu's vertical breakpoints.

12:00pm - 1:00 *billed*
* Update contact page. (2 minutes)
* On single portfolio pages, move previous/next links to menu bar. (15 minutes)
* Update header text and lists on single portfolio posts. (15 minutes)
* Update project text on portfolio index page. (15 minutes)

-----------------------------------------------------------

**Thu Dec 10 2015**

8:30pm - 9:30pm
* Catch up on Abacus.
  - Change color of <h2> in past projects modal to #414042.
  - Update portfolio category links on portfolio archive.
  - ^ This eventually needs to be automated so that it updates itself.
  - Bayard mentioned when he tried uploading photos to the photo gallery custom field he encountered a "Failed to write to disk" error, but not if he includes the photo after uploading it via the Media Library screen. I could not duplicate the error.
  - Changed sfm_category to sfm_work_category.

-----------------------------------------------------------

**Thu Nov 05 2015**

12:25pm - 12:35pm (billed)
* Make style changes to the content of single Work posts.

12:00pm - 12:10pm (billed)
* Change menu icon to SVG.

-----------------------------------------------------------

**Sat Oct 31 2015**

11:30pm - 12:00 (0.5 hours) (billed)
* Finish custom fields for single Work post content.

11:00pm - 11:30pm (0.5 hours) (billed)
* Setup custom fields for single Work post content.

10:00pm - 10:45pm (0.75 hours) (billed)
* Add SVG images.
* Update modal styles.

5:45pm - 6:30pm (0.75 hours) (billed)
* Finish styling footer.
* Style modals.

4:30pm - 5:30pm (1 hour) (billed)
* Fixed homepage.
* Style footer.
* Add padding above logo and menu button.
* Style overlays. Rewrite CSS for modals. Make more efficient and modular.

3:15pm - 3:45pm (0.5 hours) (billed)
* Fix homepage. All messed up.
  - WP Admin Bar and Footer not showing up because of missing endif;

12:50pm - 1:50pm (billed)
* Update home page design.

12:40pm - 12:50pm
* Update content for Mankato Public Safety and Profinium Place projects.
* To Do:
  - Make custom fields for "Client" --- "Location, State"
  - Make custom field for "Description. List."

-----------------------------------------------------------

**Thu Oct 29 2015**

7:15pm - 8:15pm
* Fixes for the Past Projects modal window:
  - Close button in upper right should follow scrolling.
  - Heading of each category a bit larger and maybe orange.
  - More margin above and below the footer.

7:00pm - 7:15
* Updating work pages with content from Bayard.

-----------------------------------------------------------

**Tue Oct 27 2015**

9:30pm - 11:30pm *2 hrs*
* Importing local WP content to proof site on thate.me

6:00pm - 6:30pm *0.5 hrs*
* Continue adding photos to Work posts.

5:00pm - 6:00pm *1 hr*
* Work on 'Work' pages.

-----------------------------------------------------------

**Mon Oct 26 2015**

8:00pm - 9:00pm
* Create 'Join Us' page with contact form.

-----------------------------------------------------------

**Sat Oct 24 2015**
4:00pm - 4:30pm
* Fixing "join us" buttons. Occurs two places: contact and team pages. They will look the same, more or less, but the team button is full-width and contact page is a simple button.

3:30pm - 3:45pm
* Fixed active form fields. Change :active to :focus.
* Append special class name to Abacus logo in form overlay so it has more bottom padding.
* Change color of requirement * symbol
* Removed padding from spam filter field.

12:00pm - 1:30pm
* Researching Google Maps API.
* Adding high-res photo of Keiko.

9:40am - 10:40am
* Styling contact form.

-----------------------------------------------------------

**Wed Oct 21 2015**

7:15am - 7:30am

* Adding functional map to Contact page.

-----------------------------------------------------------

**Fri Oct 9 2015**

9:00am - 11:30am/

* Styled team page.

* Change portfolio posts to match those in Bayard's mockup.

-----------------------------------------------------------

**Thu Oct 8 2015**

10:00pm - 10:30pm *Unbilled on ActiveCollab*

* Duplicated gridtile.png as home-page-texture-overlay.png, only difference is new graphic has 50% opacity.
* Reverted menu overlay opacity from 0.75 to 0.9.
* Reverted portfolio archive grid-block from large-1 to large-2 etc.

5:30pm - 6:00pm

*Task 10*
1. Create single project page.

2:30pm - 4:00pm

*Task 8*
2. x Style footer on sub-pages similarly to footer in menu overlay.

4. x Reduce space between images on Our Work page from 20px to 10px. Or, optionally, change the layout so each Our Work photo is full-screen. Decided to change block grid containing each work project to display one full-screen item per line. Looks good!

*Task 9*
1. x White bar that holds the title of each work project on archive page needs to span full width of photo, with an even margin of 10px on sides and bottom. Got it!

*Task 10*
1. x Create single project page.
  - "Previous link" doesn't exist for the first post. This causes that column to basically disappear. Need a way to fix this, as it causes the other two columns to shift to the left.
  - Need photos.
  - Need to know what the other projects are.

-----------------------------------------------------------

**Wed Oct 7 2015**

9:30pm - 10:00pm

8:30pm - 9:00pm

*Task #7*
* Lighten background of navigation overlay (by about 50%).
  x Original background alpha was 0.9. Tried 0.45, but was way too light. Settled on 0.75 for now.

*Task #8*
1. x Sub-page navigation bar change background to white with 0.9 alpha, so content can show up a little behind it as the user scrolls down the page.

2. x Botton of all sub-pages should have a footer containing "507.995.4078 / info@withabacus.com / (c) Abacus Engineering. All Rights Reserved. Good Thinking." With a divider bar at the bottom?

3. x Add more space to left and right of top bar and page content when window width decreases.

4. Reduce margin between images when they are presented. "When they are presented" means what?

-----------------------------------------------------------                                   
