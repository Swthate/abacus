<?php get_header(); ?>

<div class="row">
  <div class="large-11 medium-11 small-11 large-centered medium-centered small-centered column">

    <div class="item__title-link">
      <h1>Our Work is an Expression of Values</h1>
      <a href="<?php bloginfo('url');?>/portfolio" class="button">See Our Work</a>
    </div>

  </div> <!-- / column -->
</div> <!-- / row -->

<?php get_footer(); ?>
