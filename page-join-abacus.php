<?php

/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

get_header(); ?>



<div class="row">
	<div class="large-12 medium-11 medium-centered small-11 small-centered column columns main-content" role="main">

	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<h2 class="entry-title"><?php the_title(); ?></h2>

			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>

			<!-- Join Abacus content text -->
			<?php the_content(); ?>

			<!-- Ninja Form -->
			<div class="row">
				<div class="large-10 large-centered column">
					<?php echo do_shortcode('[gravityform id="1" title="false" description="true"]'); ?>
				</div> <!-- / column -->
			</div> <!-- / row -->

		</article>

	<?php endwhile;?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>

</div>
<?php get_footer(); ?>
