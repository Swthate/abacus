<?php

get_header(); ?>

<div class="row" id="search-filter-results-container">
	<div class="large-12 medium-11 medium-centered small-11 small-centered column main-content" role="main">

		<h2>Team.</h2>
		<p class="sub-h2">
			Abacus is made up of plumbing engineers, HVAC engineers, electrical engineers, and their respective designers and technicians. Team size is customized to meet project scale.
		</p>

		<h2>Certifications.</h2>
		<p class="sub-h2">
			DBE (Disadvantaged Business Enterprise: WBE &amp; MBE), TG (Targeted Group), NCMSDC (North Central Minority Supplier Development Council), and WBENC (Women’s Business Enterprise National Council).
		</p>

		<h2>Preferred Vendor.</h2>
		<p class="sub-h2">
			MNSCU Master Contractor / Minneapolis Public School Qualified Consultant / Macy’s Master Contractor
		</p>
</br>

  <?php
		wp_reset_query();
    $args = array(
      'post_type' => 'team',
      'order_by' 	=> 'date',
      'order' 		=> 'ASC',
    );
    $wp_query = new WP_Query( $args );
  ?>

	<?php if ( have_posts() ) : ?>
	<?php do_action( 'foundationpress_before_content' ); ?>

	<!-- Start block grid -->
	<div class="row team-members-row">

	<?php while ( have_posts() ) : the_post(); ?>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
  <?php $job_title = get_field('job_title'); ?>


		<div class="medium-4 column">
			<div class="team-member" style="background-image:url('<?php echo $thumb[0]; ?>')">

					<div class="member__details">
						<h3><?php the_title(); ?></h3>
						<p><?php echo $job_title; ?></p>
					</div>

			</div> <!-- / Team Member -->
		</div> <!-- / Post Column -->

		<div class="medium-8 column">
			<div class="black-and-white">&nbsp;</div>
		</div>


	<!-- End While Loop -->
	<?php endwhile; ?>

</div> <!-- / row -->


	<?php do_action( 'foundationpress_before_pagination' ); ?>

	<?php endif;?>

  <?php // Reset the main query ?>
  <?php wp_reset_query(); ?>



	<!-- Join -->
	<div class="row">
		<div class="large-6 medium-8 column">
			<a href="<?php bloginfo('url');?>/join-abacus" class="button join-us--block">
				<h3>Want to join the team?</h3>
				<p>Click here to inquire</p>
			</a>
		</div>
		<div class="large-6 medium-4 column">
			<div class="team-gray-box">&nbsp;</div>
		</div>
	</div> <!-- / row -->

	<!-- More photos -->
	<div class="row">
		<div class="large-4 column">
			<div class="team-photo-2">&nbsp;</div>
		</div>
		<div class="large-4 column">
			<div class="team-photo-3">&nbsp;</div>
		</div>
		<div class="large-4 column">
			<div class="team-photo-4">&nbsp;</div>
		</div>
	</div>


	<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php } ?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div> <!-- / main column -->
</div> <!-- / main row -->
</br>
<?php get_footer(); ?>
