<?php

/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

get_header(); ?>



<div class="row">
	<div class="large-12 medium-11 medium-centered small-11 small-centered column columns main-content" role="main">

	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<h2 class="entry-title"><?php the_title(); ?></h2>

			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			<p class="sub-h2">
				Abacus offices are located in Mankato, Rochester and Minneapolis, Minnesota.</p>
			<!-- Phone and Email -->
			<div class="row">
				<div class="large-12 column">
					<div class="phone-and-email">
						<h2>507.995.4078 / <a href="mailto:info@withabacus.com">info@withabacus.com</a></h2>
					</div>
				</div>
			</div>

			<!-- Offices -->
			<div class="row">
				<div class="medium-4 column offices">
					<h2>Mankato Office</h2>
					<p>
						530 North Riverfront Drive<br>
						Suite 270<br>
						Mankato, Minnesota 56001<br>
						507.995.4078
					</p>
				</div>
				<div class="medium-4 column offices">
					<h2>Rochester Office</h2>
					<p>
						4 Third Street SW<br>
						Suite 202<br>
						Rochester, MN  55902<br>
						507.995.4078
					</p>
				</div>
				<div class="medium-4 column offices">
					<h2>Minneapolis Office</h2>
					<p>
						3407 Kilmer Lane N.<br>
						Minneapolis, MN  55441<br>
						507.995.4078
					</p>
				</div>
			</div>

			<!-- Join the team -->
			<a href="<?php bloginfo('url');?>/join-abacus" class="button join-us--inline">
				<h3>Want to join the team?</h3>
				<p>Click here to inquire</p>
			</a>
</br>
			<!-- Google Map Image -->
			<!-- Responsive bits brought to you by embedresponsively.com -->
			<style>
			.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
			</style>
			<div class='embed-container'>
				<iframe src='https://www.google.com/maps/d/embed?mid=z68seDiFtb6E.kEmXSxDs8VvI&scrollbar=false' width='640' height='480' frameborder='0'></iframe>

			</div>
</br>
		</article>



	<?php endwhile;?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>

</div>
<?php get_footer(); ?>
