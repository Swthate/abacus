<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

?>

<div class="push"></div>
</section> <!-- / section class="container" role="document" FROM header.php -->


<footer>
	<div class="row">
		<div class="large-12 medium-11 medium-centered small-11 small-centered column">

			<div class="row footer__sub-page">

				<div class="large-9 medium-12 column">
					507.995.4078 / info@withabacus.com / &copy; Abacus Engineering / DBE (WBE &amp; MBE) / TG / NCMSDC / WBENC
				</div>

				<div class="large-3 medium-12 column good-thinking">
					Good Thinking.
				</div>

			</div>

		</div>
	</div>
</footer>

	<?php do_action( 'foundationpress_layout_end' ); ?>
	</div>
</div>
<?php wp_footer(); ?>

<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
