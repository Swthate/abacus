<?php $active_category = get_query_var( '_sfm_work_category' ); ?>

<ul class="nav--filter">



  <!-- All Categories -->
  <li>
    <a href="<?php bloginfo('url');?>/portfolio">
      <?php if ( !$active_category ) : ?>
        <strong>All</strong>
      <?php else : ?>
        All
      <?php endif; ?>
    </a>
  </li>



  <!-- Community & Sports Category -->
  <li>
    <a href="<?php bloginfo('url');?>/portfolio/?_sfm_work_category=Community%20%26%20Sports">
      <?php if ( $active_category === "Community & Sports" ) : ?>
        <strong>Community &amp; Sports</strong>
      <?php else : ?>
        Community &amp; Sports
      <?php endif; ?>
    </a>
  </li>



  <!-- Education Category -->
  <li>
    <a href="<?php bloginfo('url');?>/portfolio/?_sfm_work_category=Education">
      <?php if ( $active_category === "Education" ) : ?>
        <strong>Education</strong>
      <?php else : ?>
        Education
      <?php endif; ?>
    </a>
  </li>



  <!-- Food & Restaurant Category -->
  <li>
    <a href="<?php bloginfo('url');?>/portfolio/?_sfm_work_category=Food%20%26%20Restaurant">
      <?php if ( $active_category === "Food & Restaurant" ) : ?>
        <strong>Food &amp; Restaurant</strong>
      <?php else : ?>
        Food &amp; Restaurant
      <?php endif; ?>
    </a>
  </li>



  <!-- Health Care Category -->
  <li>
    <a href="<?php bloginfo('url');?>/portfolio/?_sfm_work_category=Health%20Care">
      <?php if ( $active_category === "Health Care" ) : ?>
        <strong>Health Care</strong>
      <?php else : ?>
        Health Care
      <?php endif; ?>
    </a>
  </li>



  <!-- Municipal Category -->
  <li>
    <a href="<?php bloginfo('url');?>/portfolio/?_sfm_work_category=Municipal">
      <?php if ( $active_category === "Municipal" ) : ?>
        <strong>Municipal</strong>
      <?php else : ?>
        Municipal
      <?php endif; ?>
    </a>
  </li>



  <!-- Office Category -->
  <li>
    <a href="<?php bloginfo('url');?>/portfolio/?_sfm_work_category=Office">
      <?php if ( $active_category === "Office" ) : ?>
        <strong>Office</strong>
      <?php else : ?>
        Office
      <?php endif; ?>
    </a>
  </li>



</ul>
