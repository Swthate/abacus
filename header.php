<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-precomposed.png">

		<?php wp_head(); ?>
	</head>

	<!-- If page is Home, randomize background image on body tag -->
	<?php if ( is_home() ) : ?>
		<body <?php body_class(); ?> style="background-image:url(<?php bloginfo('template_url');?>/assets/img/homepage/home_rand_<?php echo(rand(1,4));?>.jpg">
	<!-- If on any other secondary page, use default body tag -->
	<?php else : ?>
		<body <?php body_class(); ?>>
	<?php endif; ?>



		<!-- Menu Overlay -->
		<div id="menu_overlay">

			<div class="row">

				<div class="small-10 column">

					<!-- menu -->
					<?php
					$menu_args = array(
						'theme_location' 	=> 'overlay-menu',
						'container' 			=> 'nav',
						'container_class' => 'overlay-menu',
						'menu_class' 			=> 'overlay-menu__items',		// <ul>
						'menu_id' 				=> 'menu'										// <ul>
						// each <li> has class of 'menu-item'
					);
					wp_nav_menu( $menu_args );
					?>

				</div> <!-- / col -->
				<div class="small-2 column">
					<!-- exit -->
					<a id="menu_exit">
						<img src="<?php bloginfo('template_url');?>/assets/img/svg/modal_exit.svg">
					</a>
				</div>
			</div> <!-- / row -->

			<!-- menu footer -->
			<div class="row menu-footer">

				<div class="medium-9 column">
					507.995.4078 / info@withabacus.com / © Abacus Engineering. All Rights Reserved.
				</div>

				<div class="medium-3 column good-thinking">
					Good Thinking.
				</div>

			</div>

		</div> <!-- / menu_overlay -->




	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>
















<section class="container" role="document">
<?php do_action( 'foundationpress_after_header' ); ?>


<!-- Hard-coded navigation -->
<?php if ( is_home() ) : ?>
	<header class="header--home">
<?php else : ?>
	<header class="header--site">
<?php endif; ?>

		<div class="row top-logo-nav">
		<div class="large-12 medium-11 small-11 medium-centered small-centered column">



			<?php
			/**
			 * Display menu with previous/next post links for single portfolio page.
			 **/
			 if ( is_singular( 'portfolio' ) ) : ?>

			<div class="row">

				<!-- logo -->
				<div class="small-2 column">
					<a href="<?php bloginfo('url');?>">
						<img src="<?php bloginfo('template_url');?>/assets/img/svg/abacus_emblem.svg" class="logo--home">
					</a>
				</div>

				<!-- previous/next -->
				<div class="small-8 column">
					<?php include 'portfolio-prev-next.php'; ?>
				</div>

				<!-- menu -->
				<div class="small-2 column menu-link">
					<a href id="menu_enter">
						<img src="<?php bloginfo('template_url');?>/assets/img/svg/menu.svg">
					</a>
				</div>

			</div> <!-- / row -->



			<?php
			/**
			 * Display "default" menu when NOT a single portfolio page.
			 **/
			 else : ?>

			<div class="row">

			<!-- LOGO -->
			<div class="medium-6 small-8 column">
					<a href="<?php bloginfo('url');?>">
						<!-- Display Logo with Text on Home Page -->
						<?php if ( is_home() ) : ?>
							<img src="<?php bloginfo('template_url');?>/assets/img/svg/abacus_logo.svg" class="logo--home">
							<!-- Display Logo with no Text on Sub-Pages -->
						<?php else : ?>
							<img src="<?php bloginfo('template_url');?>/assets/img/svg/abacus_emblem.svg" class="logo--home">
						<?php endif; ?>
					</a>
			</div> <!-- / main column -->
			<div class="medium-6 small-4 column menu-link">
				<!-- enter -->
				<a href id="menu_enter">
					<img src="<?php bloginfo('template_url');?>/assets/img/svg/menu.svg">
				</a>
			</div> <!-- / col -->

			</div> <!-- / row -->


			<?php
			/**
			 * End if statement for deciding which menu to display.
			 **/
			 endif; ?>


		</div> <!-- large-12 medium-11 column -->
		</div> <!-- / main row -->

	</header>
