<?php

$active_category = get_query_var( '_sfm_work_category' );

if (!$active_category) {
  include ( 'parts-abacus/pro-exp__all.php' );
}
elseif ( $active_category === "Community & Sports" ) {
  include ( 'parts-abacus/pro-exp__community.php' );
}
elseif ( $active_category === "Education" ) {
  include ( 'parts-abacus/pro-exp__education.php' );
}
elseif ( $active_category === "Food & Restaurant" ) {
  include ( 'parts-abacus/pro-exp__food.php' );
}
elseif ( $active_category === "Health Care" ) {
  include ( 'parts-abacus/pro-exp__health.php' );
}
elseif ( $active_category === "Municipal" ) {
  include ( 'parts-abacus/pro-exp__municipal.php' );
}
elseif ( $active_category === "Office" ) {
  include ( 'parts-abacus/pro-exp__office.php' );
}

?>
