<?php

get_header(); ?>

<?php do_action( 'foundationpress_before_content' ); ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php include 'functions-portfolio.php'; ?>

<div class="row" id="search-filter-results-container">
	<div class="large-12 medium-11 medium-centered small-11 small-centered column main-content" role="main">

		<h2><?php the_title(); ?></h2>

			<div class="portfolio-content">

					<!-- Client / Location -->
					<p class="sub-h2">
						<?php	echo $work_client . " &mdash; " . $work_location;	?>
					</p>



					<!-- Content List -->

					<?php if( have_rows( 'work_list' ) ): ?>

						<ul class="nav--filter">

					<?php while( have_rows( 'work_list' ) ) : the_row(); ?>

						<li><?php the_sub_field( 'work_list_item' ); ?>&nbsp;</li>

					<?php endwhile; ?>

						</ul>

					<?php endif; ?>

			</div> <!-- / portfolio-content -->


			<div class="row">
				<div class="small-10 small-centered column">

				<?php
				$images = get_field('work_gallery');

				if( $images ): ?>
			    <ul class="large-block-grid-1">
		        <?php foreach( $images as $image ): ?>
	            <li>
	              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	              <p><?php echo $image['caption']; ?></p>
	            </li>
		        <?php endforeach; ?>
			    </ul>
				<?php endif; ?>

				</div> <!-- / small-10 col -->
			</div>



	<!-- End While Loop -->
	<?php endwhile; ?>



	</div> <!-- / main column -->
</div> <!-- / main row -->
<?php get_footer(); ?>
