<?php

/**
 * Custom Functions
 *
 * @author Steven Thate
 *
 **/


// Custom Post Types

function create_custom_post_types() {

  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name' => __( 'Portfolio' ),
        'singular_name' => __( 'Portfolio Item' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array(
        'title',
        'editor', // content
        'thumbnail', // featured image (big background)
        'revisions',
      )
    )
  );

  register_post_type( 'services',
    array(
      'labels' => array(
        'name' => __( 'Services' ),
        'singular_name' => __( 'Service' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array(
        'title',
        'editor', // content
        'thumbnail', // featured image (background on archive)
        'revisions',
      )
    )
  );

  register_post_type( 'team',
    array(
      'labels' => array(
        'name' => __( 'Team' ),
        'singular_name' => __( 'Team Member' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array(
        'title',
        'editor',
        'thumbnail',
        'revisions',
      )
    )
  );

}
add_action( 'init', 'create_custom_post_types' );



// Custom Menu
// fullscreen overlay

function overlay_menu() {
  register_nav_menus(
    array(
      'overlay-menu' => 'Overlay Menu' // 'code handle' => 'Menu Name in WP'
    )
  );
}
add_action( 'init', 'overlay_menu' );
//add_theme_support( 'menus' );


?>
