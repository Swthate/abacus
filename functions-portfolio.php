<?php
/**
 * Some minor functions for portfolio posts
 *
 **/

$work_category = get_field( 'work_category' );
$work_client = get_field( 'work_client' );
$work_location = get_field( 'work_location' );
$work_list = get_field( 'work_list' );

$cat_engineering  = $work_category . " Engineering";



$cat_lower = strtolower($work_category);
//Make alphanumeric (removes all other characters)
$cat_alpha = preg_replace("/[^a-z0-9_\s-]/", "", $cat_lower);
//Clean up multiple dashes or whitespaces
$cat_clean = preg_replace("/[\s-]+/", " ", $cat_alpha);
//Convert whitespaces and underscore to dash
$work_cat_id = preg_replace("/[\s_]/", "-", $cat_clean);

$modal_enter_cat = "modal_enter-" . $work_cat_id;
